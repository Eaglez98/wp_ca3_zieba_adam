<?php
/**
 *  Categories Model decleration
 */
class Categories
{
    private $category_id;
    private $genre;
    private $category_description;

    public function __construct($row)
    {
        $this->category_id          = $row['category_id'];
        $this->genre                = $row['genre'];
        $this->category_description = $row['category_description'];
    }


}
?>