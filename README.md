✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮
                        README File - AdamZieba - CA3
✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮✮

# Welcome Dear User!
This is a Game Catalogue web-application based on a MVC framework. 
Website contains few useful features which enable you to:

* View   - {Game's Catalogue},
* Add    - {Add new games into a db and assign to a category},
* Delete - {Delete games from a db & category},
* Update - {Update information about a particular game}.

Hopefully you'll be able to create your own, dream gaming catalogue which you can use as a wishlist or as a already existing library to show to your friends!

#How-to-run guide
Below I shall explain how to properly run my project (keep in mind that there is no log-in system built and everything you do is from a "Admin's" perspective).

✮ Once the project has been downloaded, you need to copy the code from `config.php.sample` to `config.php` as config/php is untracked by Git and couldn't be provided. 


/////////////////////////////////////////////
///////////////step-by-step guide///////////////
/////////////////////////////////////////////


##Build Process
✮ I have first received a MVC framework of my project (given to us from our lecturer).
---------------------------------------------------------------------------------------------------------
✮ Firstly, I created my `wp_ca3_zieba_adam` database which consists of two tables: `categories` & `games`.
![alt text](https://bitbucket.org/Eaglez98/wp_ca3_zieba_adam/src/master/assets/images/img1_db.png)
----------------------------------------------------------------------------------------------------------
✮ I then set up a new  `database.php` file (PDO) which connects my db with my MVC framework through a `confiq.php` file.
----------------------------------------------------------------------------------------------------------
✮
----------------------------------------------------------------------------------------------------------
✮
----------------------------------------------------------------------------------------------------------
/////////////////////////////////////////////
///////////////building process///////////////
/////////////////////////////////////////////



#Oops..This could work better!
Unfortunately not everything has worked the way I wanted it to work. I also didn't implement every function I had originally planned due to certain complications and perhaps lack of coding-knowledge in a certain area. Time was also an issue as I have started the project fairly late..


/////////////////////////////////////////////
///////////////list of things///////////////
/////////////////////////////////////////////

#Acknowledgments
Lastly, I would like to say Huge, Huge Thanks to my lecturer `Shane Gavin` & an ITLC support lecturer `Konstantins Loginovs` for greatly supporting and helping me out with my project through extra classes and guide-videos provided! I would also like to thank my good friend `Milosz Majewski` in 3rd year computing for explaining to me how certain concepts work.

#External Sources
✮ Tutorial Point
