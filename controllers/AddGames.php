<?php return function($req, $res) 
{
    $res->render('main', 'add-games', ['$locals']); 

    $db = new PDO 
    (
        'mysql:host=localhost;dbname=wp_ca3_zieba_adam;charset=utf8mb4',
        'root',
        ''
    );

    $stt = $db->prepare('INSERT INTO games (game_id, title, category_id, stock, game_description) values(:game_id, :title, :category_id, :stock, :game_description)');
    $stt->execute
    ([
      //Need to include data validation & sanitazation
      'game_id' => $_GET['game_id'],
      'title' => $_GET['title'],
      'category_id' => $_GET['category_id'],
      'stock' => $_GET['stock'],
      'game_description' => $_GET['game_description']
    ]);
    
    $games = $stt->fetchAll();
    print_r($games);


    $res->redirect('/home');
}
?>