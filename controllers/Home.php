<?php return function($req, $res) 
{
  $db = require_once('database.php');
  $result = $db->query('Select * from games');
  $result = $result->fetchAll(PDO::FETCH_ASSOC);

  //It calls a "main" layout and "home" view.
  $res->render('main', 'home', $result);

  
  //echo "This is home page";
  
}
?>