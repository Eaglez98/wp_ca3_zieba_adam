
<?php 
//Object which connects to a database --> PDO
//You put values that could potentially change in the future such as:
//database names etc.
    $config = require('config.php');
    $db = new PDO
    (
        //The charset argument below may be different once my db is set
        'mysql:host=localhost;dbname=' . $config['db_name'] . 
        ';charset=utf8mb4',
        $config['db_user'],
        $config['db_pass']
    );
    return $db;
?>