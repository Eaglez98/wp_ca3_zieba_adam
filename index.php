<?php
  try
  {
    define('APP_BASE_URL', '/wp_ca3_zieba_adam');
    
    //$config = require('config.php');
    //print_r($config); //may not necessary be included

    //define('APP_BASE_PATH', $config['app_base_url']);

    // Include the Rapid library
    require_once('lib/Rapid.php');

    // Create a new Router instance
    $app = new \Rapid\Router();

    //Define out routes
    //Here: requests to/back will be processed by the controller 
    //at controller/Home.php
    $app->GET('/', 'Home');
     //--------------Games/Categories Add-------------
    $app->GET('/add-categories/', 'AddCategories');
    $app->GET('/add-games/', 'AddGames');

    //MAY HAVE TO CHANGE BELOW!
    $app->POST('/add-categories/', 'AddCategories');  
    $app->POST('/add-games/', 'AddGames');
    //--------------Games/Categories Delete-------------

    // Process the request
    $app->dispatch();

  }
  catch(\Rapid\RouteNotFoundException $e)
  {
    $res = $e->getResponseObject();
    $res->status(404);
    $res->render('main', '404', []);
  }
  catch(PDOException $e)
  {

  }
  catch(Exception $e)
  {
    http_response_code(500);
    exit();
  }
?>