<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title><?= $locals['pageTitle'] ?? 'Games Catalogue'?></title>
    <link rel='stylesheet' href='<?= APP_BASEPATH ?>/assets/styles/site.css'>
</head>
<body>
<h1 class='<?= $locals['uniquePage']?'unique':''?>'>Gaming Catalogue</h1>
    <?= \Rapid\Renderer::VIEW_PLACEHOLDER ?>
    <nav>
    </nav>
</body>
</html>