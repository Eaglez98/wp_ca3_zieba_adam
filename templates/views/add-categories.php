<h2>This page adds category</h2>

<form action='' method='post'>
    <label for='categories_id'>Category ID</label>
        <input type='tinyint' id='categories_id' name='categories_id' required>

    <label for='genre'>Genre</label>
        <input type='varchar' id='genre' name='genre' required>

    <label for='category_description'>Category Description</label>
        <input type='varchar' id='category_description' name='category_description' required>

    <input type='hidden' name='action'> 
    <input type='submit' value='Add Category'>
</form>