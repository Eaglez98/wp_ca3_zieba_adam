<h2>This page adds games</h2>

<form action='' method='post'>
    <label for='game_id'>Game ID</label>
        <input type='smallint' id='game_id' name='game_id' required>

    <label for='title'>Title</label>
        <input type='text' id='title' name='title' required>

    <label for='category_id'>Category ID</label>
        <input type='tinyint' id='category_id' name='category_id' required>

    <label for='stock'>Stock</label>
        <input type='tinyint' id='stock' name='stock' required>
        
    <label for='game_description'>Game Description</label>
        <input type='varchar' id='game_description' name='game_description' required>

</form>