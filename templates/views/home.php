<h2>Welcome to your gaming catalogue!</h2>

<h2><p>Games Table</p></h2>
<table>
        <thead>
            <tr>
                <th><h2>| game_id |</h2></th>
                <th><h2>title |</h2></th>
                <th><h2>category_id |</h2></th>
                <th><h2>stock |</h2></th>
                <th><h2>game_description |</h2></th>
            </tr>
        </thead>

        <?php foreach($locals as $index => $local) { ?>
        <tbody>
            <tr><th><?=  $local['game_id']; ?></th>
                <th><?=  $local['title'];?></th>
                <th><?=  $local['category_id'];?></th>
                <th><?=  $local['stock'];?></th>
                <th><?=  $local['game_description'];?></th>
        </tbody>
        <?php }?>
</table>
<li><a href='<?= APP_BASE_URL ?>/add-games'>Add Games</a></li>

<!-- <form action='' method='post'>
    <label for='game_id'>Game ID</label>
        <input type='smallint' id='game_id' name='game_id' required>

    <label for='title'>Title</label>
        <input type='text' id='title' name='title' required>

    <label for='category_id'>Category ID</label>
        <input type='tinyint' id='category_id' name='category_id' required>

    <label for='stock'>Stock</label>
        <input type='tinyint' id='stock' name='stock' required>
        
    <label for='game_description'>Game Description</label>
        <input type='varchar' id='game_description' name='game_description' required>

    <input type='hidden' name='action'> 
    <input type='submit' value='Add Game'>
</form> -->


<h2><p>Categories Table</p></h2>
<table>
        <thead>
            <tr>
                <th><h2>| categories_id |</h2></th>
                <th><h2>genre |</h2></th>
                <th><h2>category_description |</h2></th>
            </tr>
        </thead>

        <?php foreach($locals as $index => $local) { ?>
        <tbody>
            <tr><th><?= $local['categories_id']; ?></th>
                <th><?= $local['genre'];?></th>
                <th><?= $local['category_description'];?></th>
        </tbody>
        <?php }?>
</table>
<li><a href='<?= APP_BASE_URL ?>/add-categories'>Add Categories</a></li>

<!-- <form action='' method='post'>
    <label for='categories_id'>Category ID</label>
        <input type='tinyint' id='categories_id' name='categories_id' required>

    <label for='genre'>Genre</label>
        <input type='varchar' id='genre' name='genre' required>

    <label for='category_description'>Category Description</label>
        <input type='varchar' id='category_description' name='category_description' required>

    <input type='hidden' name='action'> 
    <input type='submit' value='Add Category'>
</form> -->

